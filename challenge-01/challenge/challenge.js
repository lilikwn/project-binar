// Nama : Lilik Wahyu Nugroho
// Kelas : Backend Javascript
// Email : lilik1.wahyu7@gmail.com / 111201912183@mhs.dinus.ac.id

// Input module for get user input in terminal
const readline = require('readline');
const process = require('process');

const interface = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

// function for get data about calculation history
const history = {
  historyData : [],
  addHistory: (data) => {
    history.historyData.push(data)
  },
  getHistory: ()=>{
    const {historyData} = history;
    if(historyData.length){
      historyData.forEach(data => {
        console.log(` > ${data.type} >> ${data.input} = ${data.result}`);
      })
    }else{
      console.log("Tidak ada history")
    }
  }
}

// function to get input from user
const input = (question) => {
  return new Promise(resolve => {
    interface.question(question, (data) => {
      return resolve(data)
    })
  })
}

// function to separate values ​​with arithmetic operators
const getInputData = (input, splitter) => {
  const data = input.split(splitter).map(angka => +angka);
  return data;
}

// Calculator Functions
const calculator = {
  tambah : (input) =>{
    let total = 0;
    input.forEach(data => total += data);
    return total;
  },
  kurang : (input)=>{
    let total=input[0];
    for(i = 1; i<input.length; i++){
      total -= input[i]
    }
    return total;
  },
  kali : (input)=>{
    let total = 1;
    input.forEach(data => total *= data);
    return total;
  },
  bagi : (input) => {
    let total=input[0];
    for(i = 1; i<input.length; i++){
      total /= input[i]
    }
    return total;
  },
  akarKuadrat : (nilai) => {
    return Math.sqrt(nilai)
  },
  volumeTabung: (radius, height)=>{
    return 3.14 * (radius**2) * height;
  }
}

const {tambah, kurang, kali, bagi, akarKuadrat, volumeTabung} = calculator

// Menu List
const dataMenu = ["Penjumlahan", "Pengurangan", "Perkalian", "Pembagian", "Akar Kuadrat", "Luas Persegi", "Volume Kubus", "Volume Tabung", "History Calculator"]


// Function to display menu
const menu = async () => {
  try {
    console.log("===================")
    console.log("| Calculator Menu |");
    console.log("===================")
    console.log("")
    dataMenu.forEach((menu, index) => {
      console.log(`${++index} ${menu}`)
    })
    let pilihan = await input("Pilih Operasi (1 sampai 9): ")
    process.stdout.write('\033c');
    switch(pilihan){
      case "1":
        {
          console.log("Masukkan input dengan format: [nilai1]+[nilai2]+[nilai3] dst...")
          console.log("contoh : 10+20+30")
          const inputUser = await input(">>> ");
          const data = getInputData(inputUser, "+");
          const result = tambah(data);
          console.log("Hasil : ", result);
          history.addHistory({
            type: "Penjumlahan",
            input: inputUser,
            result: result
          })
          break;
        }
      case "2":
        {
          console.log("Masukkan input dengan format: [nilai1]-[nilai2]-[nilai3] dst...")
          console.log("contoh : 30-20-10")
          const inputUser = await input(">>> ");
          const data = getInputData(inputUser, "-");
          const result = kurang(data);
          console.log("Hasil : ", result);
          history.addHistory({
            type: "Pengurangan",
            input: inputUser,
            result: result
          })
          break;
        }
      case "3":
        {
          console.log("Masukkan input dengan format: [nilai1]x[nilai2]x[nilai3] dst...")
          console.log("contoh : 2x3x4")
          const inputUser = await input(">>> ");
          const data = getInputData(inputUser, "x");
          const result = kali(data);
          console.log("Hasil : ", result)
          history.addHistory({
            type: "Perkalian",
            input: inputUser,
            result: result
          })
          break;
        }
      case "4":
        {
          console.log("Masukkan input dengan format: [nilai1]/[nilai2]/[nilai3] dst...")
          console.log("contoh : 40/10/5")
          const inputUser = await input(">>> ");
          const data = getInputData(inputUser, "/");
          const result = bagi(data);
          console.log("Hasil : ", result);
          history.addHistory({
            type: "Pembagian",
            input: inputUser,
            result: result
          })
          break;
        }
      case "5":
        {
          const inputUser = await input(" masukkan nilai >>> ");
          const result = akarKuadrat(+inputUser);
          console.log("Hasil : ", result)
          history.addHistory({
            type: "Akar Kuadrat",
            input: inputUser,
            result: result
          })
          break;
        }
      case "6":
        {
          const inputUser = await input(" masukkan nilai sisi persegi >>> ");
          const result = (+inputUser)**2;
          console.log("Hasil : ", result)
          history.addHistory({
            type: "Luas Persegi",
            input: `sisi persegi ${inputUser}`,
            result: result
          })
          break;
        }
      case "7":
        {
          let inputUser = await input("masukkan nilai rusuk kubus >>> ");
          inputUser = +inputUser;
          const result = inputUser**3;
          console.log("Hasil : ", result);
          history.addHistory({
            type: "Volume Kubus",
            input: `rusuk persegi ${inputUser}`,
            result: result
          })
          break;
        }
      case "8":
        {
          let radius = await input(" Radius Tabung >>> ");
          let tinggi = await input(" Tinggi Tabung >>> ");
          const result = volumeTabung(+radius, +tinggi)
          console.log("Hasil : ", result);
          history.addHistory({
            type: "Volume Tabung",
            input: `radius: ${radius}, tinggi: ${tinggi}`,
            result: result
          })
          break;
        }
      case "9":
        {
          history.getHistory();
          break;
        }
      default:
        {
          console.log("Pilihan tidak tersedia")
        }
    }
    // condition to ask user want to use the app again or not
    const exit = await input("Apakah anda ingin melakukan perhitungan lagi ? (y/n) >> ")
    if(exit === "y" || exit === "Y"){
      process.stdout.write('\033c');
      menu();
    }else{
      interface.close()
    }
  } catch (error) {
    return error
  }
}

// main Function
const main = async () => {
  await menu();
}

// Run Main Function
main();