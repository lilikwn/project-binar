const vapidPublicKey = "BOBayWwuFzEFnNNg-9J7uLudPu78rEdrfbPQ92tY8Sz10pqs6CWD7dZKPS660GX8ePacbcs-y2cbJYkB9g_rEwk"

const send = async () => {

  // Register service worker
  console.log('Registering service worker....')
  const register = await navigator.serviceWorker.register('/worker.js', {
    scope: '/'
  });
  console.log('Service Worker Registered...')

  // Register push
  console.log('registering push....');
  const subscription = await register.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: vapidPublicKey
  })
  console.log('push registered')

  // Send Push notification
  console.log('sending push...')
  await fetch('/subscribe', {
    method: 'POST',
    body: JSON.stringify(subscription),
    headers: {
      'content-type': 'application/json'
    }
  });
  console.log('push sent');


}

if('serviceWorker' in navigator){
  send().catch(err=> console.error(err));
}