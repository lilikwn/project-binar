require('dotenv').config();
const express = require('express');
const app = express();
const port = 3000;
const userRouter = require('./routes/user');
const authRouter = require('./routes/auth');
const biodataRouter = require('./routes/biodata');
const historyRouter = require('./routes/history');
const uploadRouter = require('./routes/media');
const paymentRouter = require('./routes/payment')
const {mustLogin, isUserExist, isBiodataExist} = require('./helper/middleware');
const morgan = require('morgan');
const rbac = require('./utils/rbac/');
const webpush = require('web-push');

const {
  VAPID_PUBLIC_KEY,
  VAPID_PRIVATE_KEY
} = process.env;

webpush.setVapidDetails('mailto:test@test.com', VAPID_PUBLIC_KEY, VAPID_PRIVATE_KEY);

app.set('view engine', 'ejs')
app.use(express.static('./public'));
app.use(morgan('dev'));
app.use(express.urlencoded({extended: true}))
app.use(express.json());
app.use('/users', userRouter);
app.use('/auth', authRouter);
app.use('/:userId/biodata', mustLogin, isUserExist, biodataRouter);
app.use('/:userId/history', mustLogin, isUserExist, historyRouter);
app.use('/:userId/upload', mustLogin, rbac(), isUserExist, uploadRouter);
app.use('/payment', paymentRouter);

app.listen(port, '127.0.0.1', ()=>{
  console.log(`Connected to ${port}`);
})

module.exports = app;