const {register, login, google: googleLogin, facebook: facebookLogin, forgotPassword, resetPassword, registerPage} = require('../controller/auth');
const router = require('express').Router();
const oauth = require('../controller/auth');

router.post('/register', register);
router.get('/register', registerPage);
router.post('/login', login);
router.get('/login/google', googleLogin);
router.get('/login/facebook', facebookLogin);
router.post('/forgot-password', forgotPassword);
router.post('/reset-password', resetPassword);
router.get('/reset-password', async (req, res) => {
  const {token} = req.query;
  const url = `http://localhost:3000/auth/reset-password?token=${token}`
  res.render('pageResetPassword', {url})
})

module.exports = router;