const express = require('express');
const { getUserGameHistory, createUserGameHistory, updateUserGameHistory, deleteGameHistory } = require('../controller/gameHistory');
const router = express.Router({mergeParams:true});
const rbac = require('../utils/rbac');
const roles = require('../utils/users/role')

router.get('/', rbac(roles.user), getUserGameHistory);
router.post('/', rbac(roles.admin), createUserGameHistory);
router.put('/:historyId', rbac(roles.admin), updateUserGameHistory);
router.delete('/:historyId', rbac(roles.admin), deleteGameHistory);

module.exports = router;