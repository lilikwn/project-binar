const express = require('express');
const router = express.Router({mergeParams:true});
const uploadVideo = require('../utils/media-handling/videos');
const uploadImage = require('../utils/media-handling/images');
const path = require('path');
const rbac = require('../utils/rbac');
const {Media} = require('../models');
const {user_game} = require('../models');


router.post('/video', rbac('Admin'), uploadVideo.single('video'), async (req, res)=>{
  const {userId} = req.params;

  const findUser = await user_game.findOne({
    where: {
      id: userId
    }
  })

  if(!findUser){
    return res.status(404).json({
      status: false,
      message: 'user notfound'
    })
  }
  const fileUrl = `${req.protocol}://${req.get('host')}/videos/${req.file.filename}`;
  const newMedia = await Media.create({
    filename: req.file.filename,
    file_url: fileUrl,
    user_id: userId,
    media_type: path.extname(req.file.filename)
  })

  return res.json({
    url: newMedia.file_url
  })
})

router.post('/image', rbac('Admin'), uploadImage.single('image'), async(req, res)=>{
  const {userId} = req.params;

  const findUser = await user_game.findOne({
    where: {
      id: userId
    }
  })

  if(!findUser){
    return res.status(404).json({
      status: false,
      message: 'user notfound'
    })
  }
  const fileUrl = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`;
  const newMedia = await Media.create({
    filename: req.file.filename,
    file_url: fileUrl,
    user_id: userId,
    media_type: path.extname(req.file.filename)
  })

  return res.json({
    url: newMedia.file_url
  })
})

router.get('/', rbac(), async(req, res) => {
  const {user_id, media_type} = req.query;
  const {userId} = req.params;

  const findUser = await user_game.findOne({
    where: {
      id: userId
    }
  })

  if(!findUser){
    return res.status(404).json({
      status: false,
      message: 'user notfound'
    })
  }

  let media = null;

  if(user_id && media_type) {
    media = await Media.findAll({
      where: {
        user_id,
        media_type 
      }
    })
  } else if(user_id) {
    media = await Media.findAll({
      where: {
        user_id
      }
    })
  } else if(media_type) {
    media = await Media.findAll({
      where: {
        media_type 
      }
    })
  } else if(!media_type && !user_id){
    media = await Media.findAll()
  }

  if(!media){
    return res.status(404).json({
      status: false,
      message: 'media not found'
    })
  }

  return res.status(200).json({
    status: true,
    message: 'success to get media',
    data: media,
  })
  
})


module.exports = router;