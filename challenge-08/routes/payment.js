const router = require('express').Router();
const {paymentConfirmation} = require('../controller/payment')

router.post('/confirmation', paymentConfirmation);
router.get('/success', async (req, res) => {
  const {token} = req.query;
  const url = `http://localhost:3000/payment/success?token=${token}`
  res.render('paymentSuccess', {url})
})

module.exports = router;