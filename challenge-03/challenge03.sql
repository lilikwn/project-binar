--

-- CREATE TABLE

-- create table user_game
CREATE TABLE user_game(
  id BIGSERIAL PRIMARY KEY,
  username varchar(18) NOT NULL,
  password varchar(32) NOT NULL,
  email varchar(64) NOT NULL
);

-- create table user_game_biodata
CREATE TABLE user_game_biodata(
  id BIGSERIAL PRIMARY KEY,
  user_id INTEGER NOT NULL,
  avatar VARCHAR(255),
  nickname VARCHAR(32) NOT NULL,
  rank varchar(32) NOT NULL,
  level INTEGER NOT NULL
);

-- create table user_game_history
CREATE TABLE user_game_history(
  id BIGSERIAL PRIMARY KEY,
  user_id INTEGER NOT NULL,
  score INTEGER NOT NULL,
  is_win BOOLEAN NOT NULL,
  match_type VARCHAR(32) NOT NULL,
  duration INTEGER NOT NULL
);

-- INSERT DATA

--insert data to table user_game
INSERT INTO
  user_game(username, password, email)
VALUES
  ('mikasa_ackerman', 'ini_password', 'mikasa2000@mail.com'),
  ('Gon_D_Rong', 'gondrong123', 'gondrong17@mail.com'),
  ('lilikwn', 'helloworld', 'lilikwn@mail.com'),
  ('newbee', 'inibudi123', 'inibudi@mail.com'),
  ('newplayer', 'akunbaru', 'emailbaru@mail.com');


--insert data to table user_game_biodata
INSERT INTO
  user_game_biodata(user_id, avatar, nickname, rank, level)
VALUES
  (1, '/avatar/mikasa_ackerman.png', 'mikasa', 'Mythical Glory', 40),
  (2, '/avatar/Gon_D_Rong.png', 'gotoproplayer', 'Epic', 40),
  (3, '/avatar/lilikwn.png', 'lilik', 'Legend', 40),
  (4, '/avatar/newbee.png', 'newbee', 'Elite', 15),
  (5, '/avatar/newplayer.png', 'barumainbang', 'Warior', 2);

--insert data to table user_game_history
INSERT INTO
  user_game_history(user_id, score, is_win, match_type, duration)
VALUES
  (1, 8, true, 'Rank', 1200),
  (1, 9, true, 'Rank', 900),
  (1, 7, true, 'Rank', 1440),
  (2, 5, false, 'Rank', 660),
  (2, 6, true, 'Classic', 960),
  (3, 8, true, 'Rank', 720),
  (4, 3, false, 'Classic', 600);

-- READ DATA

-- READ DATA nickname + rank
SELECT nickname, rank FROM user_game_biodata;
-- result

--    nickname    |      rank      
-- ---------------+----------------
--  mikasa        | Mythical Glory
--  gotoproplayer | Epic
--  lilik         | Legend
--  newbee        | Elite
--  barumainbang  | Warior

-- UPDATE DATA

-- update password user 1 
UPDATE user_game 
SET password = 'passwordkubaru098'
WHERE id = 1;

-- DELETE DATA

-- Delete user with id = 2
DELETE FROM user_game WHERE id = 2;

-- Delete history game with user_id = 2;
DELETE FROM user_game_history WHERE user_id = 2;

-- Delete Biodata Game with user_id = 2;
DELETE FROM user_game_biodata WHERE user_id = 2;
