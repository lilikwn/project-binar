const output = (daftarNilai) => {
  const max = Math.max(...daftarNilai);
  const min = Math.min(...daftarNilai);
  const average =  (daftarNilai.reduce((a, b) => a+b, 0) / daftarNilai.length).toFixed(2)
  const siswaLulus = daftarNilai.filter((nilai) => nilai >= 60);
  const siswaTidakLulus = daftarNilai.filter((nilai) => nilai < 60);
  const siswa90Dan100 = daftarNilai.filter((nilai) => nilai == 90 || nilai == 100);
  const sortNilai = daftarNilai.sort((a, b) => a-b);

  console.log(`Nilai tertinggi: ${max}`);
  console.log(`Nilai terendah: ${min}`);
  console.log(`Siswa lulus: ${siswaLulus.length}, Siswa tidak lulus: ${siswaTidakLulus.length}`);
  console.log(`Urutan nilai dari terendah ke tertinggi: ${sortNilai}`);
  console.log(`Siswa nilai 90 dan nilai 100: ${siswa90Dan100}`);
  console.log(`Rata-rata nilai siswa : ${average}`)
}


module.exports = {output}