const readline = require('readline');
const process = require('process');


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const input = (question) => {
  return new Promise(resolve => {
    rl.question(question, (data) => {
      return resolve(data)
    })
  })
}

module.exports = {input, rl}