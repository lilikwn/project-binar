const {register, login, google: googleLogin, facebook: facebookLogin} = require('../controller/auth');
const router = require('express').Router();
const oauth = require('../controller/auth');

router.post('/register', register);
router.post('/login', login);
router.get('/login/google', googleLogin);
router.get('/login/facebook', facebookLogin);

module.exports = router;