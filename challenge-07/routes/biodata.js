const express = require('express');
const router = express.Router({mergeParams: true});
const { getBiodata, createBiodata, updateBiodata, deleteBiodata } = require('../controller/biodata');
const { isUserExist, isBiodataExist } = require('../helper/middleware');
const rbac = require('../utils/rbac');
const roles = require('../utils/users/role')

router.get('/', rbac(roles.user), isBiodataExist, getBiodata);
router.post('/', rbac(roles.admin), createBiodata);
router.put('/', rbac(roles.admin), isBiodataExist, updateBiodata);
router.delete('/', rbac(roles.admin), isBiodataExist, deleteBiodata);

module.exports = router;