const express = require('express');
const router = express.Router();
const user = require('../controller/user');
const rbac = require('../utils/rbac');
const roles = require('../utils/users/role')

router.get('/', rbac(roles.user), user.getAllData);
router.get('/:userId', rbac(roles.user), user.getDetailUser);
router.post('/', rbac(roles.admin), user.createNewUser);
router.put('/:userId', rbac(roles.admin), user.updateDataUser);
router.delete('/:userId', rbac(roles.admin), user.deleteDataUser);

module.exports = router;