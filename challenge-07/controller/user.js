const {user_game} = require('../models');
const bcrypt = require('bcrypt');
const SaltRounds = 10;
const userType = require('../utils/users/type')
const roles = require('../utils/users/role')

module.exports = {
  getAllData : async (req, res) => {
    try {
      const users = await user_game.findAll();
      if(users.length < 1 ){
        return res.status(404).json({
          status: 'failed',
          message: 'data not found',
        })
      }
      return res.status(200).json({
        status: 'success',
        message: 'success to get All data',
        data: users,
      })
    } catch (error) {
      console.log(error);
    }
  },
  getDetailUser: async(req, res) => {
    const {userId} = req.params;
    const user = await user_game.findOne({
      where: {id: userId}
    });
    if(!user){
      return res.status(404).json({
        status: 'failed',
        message: 'data not found',
      })
    }
    return res.status(200).json({
      status: 'success',
      message: 'success to get specific user data',
      data: user
    })
  },
  createNewUser: async(req, res) => {
    const {email, password, name, role = roles.user} = req.body;
    const emailExist = await user_game.findOne({
      where: {email}
    })
    if(emailExist){
      return res.status(400).json({
        status: 'failed',
        message: 'email already used',
      })
    }
    const encryptedPassword = await bcrypt.hash(password, SaltRounds);
    const user = await user_game.create({
      name,
      email,
      password: encryptedPassword,
      role,
      user_type: userType.basic
    })
    return res.status(201).json({
      status: 'success',
      message: 'success to added new data user',
      data: {
        id: user.id,
        email: user.email,
        password: user.password,
        name: user.name,
        user_type: user.user_type,
        role,
        updatedAt: user.updatedAt,
        createdAt: user.createdAt,
      }
    })
  }, updateDataUser: async(req, res) => {
    const {email, password} = req.body;
    const {userId} = req.params;
    const encryptedPassword = await bcrypt.hash(password, SaltRounds);
    const user = await user_game.findOne({
      where: {id: userId}
    });
    if(!user){
      return res.status(404).json({
        status: 'failed',
        message: 'data not found',
      })
    }
    await user_game.update({
      email,
      password: encryptedPassword
    }, {
      where: {id: userId}
    })
    return res.status(200).json({
      status: 'success',
      message: 'success to edit data user',
    })
  }, deleteDataUser: async(req, res) => {
    const {userId} = req.params;
    const user = await user_game.findOne({
      where: {id: userId}
    });
    if(!user){
      return res.status(404).json({
        status: 'failed',
        message: 'data not found',
      })
    }
    await user_game.destroy({
      where: {id: userId}
    });
    return res.status(200).json({
      status: 'success',
      message: 'success to delete user'
    });
  }
}