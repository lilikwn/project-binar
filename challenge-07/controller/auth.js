const { createNewUser } = require("./user");
const bcrypt = require("bcrypt");
const { user_game } = require("../models");
const jwt = require("jsonwebtoken");
const userType = require("../utils/users/type");
const googleOauth2 = require("../utils/oauth/google");
const facebookOauth2 = require('../utils/oauth/facebook');
const roles = require("../utils/users/role");
const { user_game: user } = require("../models");

const { JWT_SECRET_KEY } = process.env;

module.exports = {
  register: createNewUser,
  login: async (req, res) => {
    const { email, password } = req.body;
    const user = await user_game.findOne({
      where: { email },
    });

    if (!user) {
      return res.status(404).json({
        status: "failed",
        message: "email not found",
      });
    }

    if (user.user_type != userType.basic) {
      return res.status(400).json({
        status: false,
        message: `your account is asscoiated with ${user.user_type}`,
        data: null,
      });
    }

    const passwordIsMatch = await bcrypt.compare(password, user.password);
    if (!passwordIsMatch) {
      return res.status(403).json({
        status: "failed",
        message: "email and password didn't match",
      });
    }

    const payload = {
      email: user.email,
      role: user.role,
      user_type: user.user_type,
      loginDate: new Date().toISOString(),
    };

    const token = jwt.sign(payload, JWT_SECRET_KEY);

    return res.status(200).json({
      status: "success",
      message: "login success",
      data: {
        user_id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
        token,
      },
    });
  },
  google: async (req, res, next) => {
    try {
      const code = req.query.code;

      if (!code) {
        const url = googleOauth2.generateAuthURL();
        return res.redirect(url);
      }

      // get token
      await googleOauth2.setCredentials(code);

      const { data } = await googleOauth2.getUserData();

      // check apakah user email ada di database
      let userExist = await user.findOne({ where: { email: data.email } });

      if (!userExist) {
        userExist = await user.create({
          name: data.name,
          email: data.email,
          user_type: userType.google,
          role: roles.user,
        });
      }

      // generate token
      const payload = {
        id: userExist.id,
        name: userExist.name,
        email: userExist.email,
        user_type: userExist.user_type,
        role: userExist.role,
      };
      const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

      // return token
      return res.status(200).json({
        status: true,
        message: "success",
        data: {
          user_id: userExist.id,
          token,
        },
      });
    } catch (err) {
      next(err);
    }
  },
  facebook: async (req, res, next) => {
    try {
      const code = req.query.code;

      // form login jika code tidak ada
      if (!code) {
        const url = facebookOauth2.generateAuthURL();
        return res.redirect(url);
      }

      // acces_token
      const access_token = await facebookOauth2.getAccessToken(code);

      // get user info
      const userInfo = await facebookOauth2.getUserInfo(access_token);

      // check apakah user email ada di database
      let userExist = await user_game.findOne({
        where: { email: userInfo.email },
      });

      // if !ada -> simpan data user
      if (!userExist) {
        userExist = await user_game.create({
          name: [userInfo.first_name, userInfo.Last_name].join(" "),
          email: userInfo.email,
          user_type: userType.facebook,
          role: roles.user,
        });
      }

      // generate token
      const payload = {
        id: userExist.id,
        name: userExist.name,
        email: userExist.email,
        user_type: userExist.user_type,
        role: roles.user,
      };
      const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

      // return token
      return res.status(200).json({
        status: true,
        message: "success",
        data: {
          user_id: userExist.id,
          token,
        },
      });
    } catch (err) {
      next(err);
    }
  },
};
