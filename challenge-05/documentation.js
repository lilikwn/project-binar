const {Collection} = require('postman-collection');
const {loginRequest, registerRequest} = require('./documentation/auth');
const {createBiodata, getBiodata, updateBiodata, deleteBiodata} = require('./documentation/biodata');
const {createHistory, getHistory, updateHistory, deleteHistory} = require('./documentation/gameHistory');
const {createUser, getUser, getUsers, updateUser, deleteUser} = require('./documentation/user')
const fs = require('fs');


const postmanCollection = new Collection({
  info: {
    name: 'Game API Documentation'
  },
  item: []
});

// Auth
postmanCollection.items.add(loginRequest);
postmanCollection.items.add(registerRequest);

// Biodata
postmanCollection.items.add(createBiodata);
postmanCollection.items.add(getBiodata);
postmanCollection.items.add(updateBiodata);
postmanCollection.items.add(deleteBiodata);

// History Game
postmanCollection.items.add(createHistory);
postmanCollection.items.add(getHistory);
postmanCollection.items.add(updateHistory);
postmanCollection.items.add(deleteHistory);

// Users
postmanCollection.items.add(createUser);
postmanCollection.items.add(getUsers);
postmanCollection.items.add(getUser);
postmanCollection.items.add(updateUser);
postmanCollection.items.add(deleteUser);


// convert to json
const collectionJSON = postmanCollection.toJSON();

// export to file
fs.writeFile('./collection.json', JSON.stringify(collectionJSON), (err)=>{
  if (err) console.log(err);
  console.log('file saved')
});
